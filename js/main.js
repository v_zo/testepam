var resArr;
var url = 'https://api.randomuser.me/1.0/?results=51&nat=gb,us&inc=gender,name,location,email,phone,picture';
getUsers(url);

document.addEventListener('keydown', function (event) {
    const key = event.key;
    if (key === "Escape") {
        closePopup();
    }
});

function getUsers(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.send(null);
    xhr.onreadystatechange = function () {
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            document.getElementById('loader').style.display = "none";
            document.getElementById('people-list-id').innerHTML = "<h2>No internet</h2>";
        } else {
            resArr = (JSON.parse(xhr.responseText)).results;
            showUsersList();
            document.getElementById('loader').style.display = "none";
            document.getElementById('sorting-container').style.display = "flex";
        }
    }
}

function showUsersList() {
    var peopleList = document.getElementById('people-list-id');
    while (peopleList.firstChild) {
        peopleList.removeChild(peopleList.firstChild);
    }
    var selectProp = document.getElementById('select-prop');
    sortUsers(resArr, selectProp.options[selectProp.selectedIndex].value);
    resArr.forEach(element => {
        var userElement = peopleList.appendChild(document.createElement('div'));
        createCard(userElement, element);
    });
}

function sortUsers(arr, prop) {
    var selectDirection = document.getElementById('select-direction');
    var direction = Number(selectDirection.options[selectDirection.selectedIndex].value);
    arr.sort(
        function (a, b) {
            if (prop != 'gender') {
                a = a.name;
                b = b.name;
            }
            if (a[prop] > b[prop]) {
                return 1 * direction;
            }
            if (a[prop] < b[prop]) {
                return -1 * direction;
            }
            return 0;
        }
    );
}

function createCard(userElement, userData) {
    var container = userElement.appendChild(document.createElement('div'));
    var usrAvatar = container.appendChild(document.createElement('div'));
    var usrName = container.appendChild(document.createElement('div'));
    container.classList.add("person-container");
    usrName.classList.add("person-name");
    usrAvatar.classList.add("person-avatar");
    usrAvatar.style.backgroundImage = `url( ${userData.picture.medium} )`;
    var N = userData.name;
    usrName.innerHTML = `${N.title} ${N.first.capitalize()} ${N.last.capitalize()}`;
    container.addEventListener('click', function () {
        personClick(userData);
    });
}

//a bit shorter
function getEl(className) {
    return document.getElementsByClassName(className)
};

function personClick(userData) {
    getEl('card-image')[0].src = `${userData.picture.large}`;
    var N = userData.name;
    var nameString = `${N.title}. ${N.first.capitalize()} ${N.last.capitalize()}`;
    getEl('card-user-name')[0].innerHTML = nameString;
    getEl('card-image')[0].alt = nameString;
    var L = userData.location;
    getEl('card-adress')[0].innerHTML = '<i class="fa fa-home"></i>  ' + `${L.street.capitalize()}, ${L.city.capitalize()}, ${L.state.capitalize()}`;
    getEl('card-e-mail')[0].innerHTML = '<i class="fa fa-envelope"></i>  ' + userData.email;
    getEl('card-phone-number')[0].innerHTML = '<i class="fa fa-phone"></i>  ' + userData.phone;
    getEl("card-wrapper")[0].style.visibility = "visible";
    getEl("card")[0].className += " opacity";
}

function closePopup() {
    var popup = getEl("card-wrapper")[0];
    if (popup.style.visibility != "hidden") {
        popup.style.visibility = "hidden";
    }
    getEl("card")[0].classList.remove("opacity");
    getEl("card-image")[0].src = "";
}

String.prototype.capitalize = function (a) {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
